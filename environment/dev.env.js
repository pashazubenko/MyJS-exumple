module.exports = {
  ENV: '"development"',
  NODE_ENV: '"development"',
  DEBUG_MODE: true,

  PATH_HOME: '"https://customers.dev/"',
  PATH_BASE: '""',

  // Auth
  AUTH_CLIENT_ID: '""',
  AUTH_DOMAIN: '""',

  // APIs
  CUSTOMER_API_URL: '""',
  TRANSLATIONS_API_URL:'""',

  DEFAULT_LOCALE:'"nl"'
}
