import { expect, assert } from 'chai';
import CustomerApiProvider  from './CustomerApiProvider';

class MockCustomerApiProvider extends CustomerApiProvider {
  constructor() {
    super();
    this.http = {
      get: () => {
        return Promise.resolve({
          data: [
            {
              'tour_date': '2017-11-22'
            }
          ]
        });
      }
    };
  }
}


describe('CustomerApiProvider tests', () => {

  let customer_api_provider: CustomerApiProvider;

  beforeEach(() => {
    customer_api_provider = new MockCustomerApiProvider();
  });

  it('CustomerApiProvider getBookingsHistory', async () => {
    let promise = customer_api_provider.getBookingsHistory();

    let history = await promise;

    history.data.map((order) => {
      expect(order.tour_date).to.be.a('string');
      assert.equal(order.tour_date, '2017-11-22');
    });
  });
});

