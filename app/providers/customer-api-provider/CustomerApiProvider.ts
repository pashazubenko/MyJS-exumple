import axios from 'axios';
import CustomerApiProviderInterface from '../../interfaces/CustomerApiProviderInterface';
import Container from '../../container/IocContainer';
import SERVICES from '../../services/services';
import Auth from '../../interfaces/Auth';
import TranslationProviderInterface from '../../interfaces/TranslationProviderInterface';
import { injectable } from 'inversify';

@injectable()
class CustomerApiProvider implements CustomerApiProviderInterface {

  /**
   * @type {Auth}
   */
  protected auth_provider = Container.get<Auth>(SERVICES.AUTH);
  protected translationProvider = Container.get<TranslationProviderInterface>(SERVICES.TRANSLATION);

  /**
   *
   */
  protected http;

  /**
   *
   */
  constructor () {
    let locale = this.translationProvider.getCurentLocale();
    this.http = axios.create({
      baseURL: process.env.CUSTOMER_API_URL,
      headers: {
        Authorization: 'Bearer ' + this.auth_provider.token(),
        Language: locale
      }
    });
  }

  /**
   * @returns {Promise<any>}
   */
  public async getBookingsHistory() {
    return await this.http.get('v1.0/bookings?sort=tour_date&order=desc&limit=3');
  }

  /**
   * @returns {Promise<any>}
   */
  public async getBookingsHistoryPage(next_page) {
    return await this.http.get('v1.0/bookings?sort=tour_date&order=desc&limit=3&page=' + next_page );
  }

  /**
   * @returns {Promise<any>}
   */
  public async setUserInfo(user) {
    return await this.http.patch('v1.0/profile', user);
  }

  /**
   * @returns {Promise<any>}
   */
  public async getUserInfo() {
    return await this.http.get('v1.0/profile');
  }

  /**
   * @returns {Promise<any>}
   */
  public async getCountries() {
    return await this.http.get('/v1.0/countries');
  }

  /**
   * @returns {Promise<any>}
   */
  public async getUserSettings() {
    return await this.http.get('/v1.0/profile?fields=settings');
  }

  /**
   * @returns {Promise<any>}
   */
  public async getUserReviews() {
      return await this.http.get('/v1.0/reviews?status=HIDDEN,ON CUSTOMER REVIEW,PENDING');
  }
}

export default CustomerApiProvider;
