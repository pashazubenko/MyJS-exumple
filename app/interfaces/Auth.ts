interface Auth {
  auth0;

  login ();
  logout ();
  token();
  authPromise();
  isAuthenticated();
}
export default Auth;
