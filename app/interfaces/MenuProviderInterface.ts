interface MenuProviderInterface {
    getMenus(locale);
}
export default MenuProviderInterface;
