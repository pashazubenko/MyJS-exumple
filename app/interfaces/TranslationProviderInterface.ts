interface TranslationProviderInterface {
    getTranslation(locale);
    getCurentLocale();
}
export default TranslationProviderInterface;
