interface Format {
    dateMonthYear(time, locale);
    monthDateYear(time, locale);
    time(time);
    fullDate(time);
    date(time, locale);
}
export default Format;
