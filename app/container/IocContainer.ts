import 'reflect-metadata';
import { Container } from 'inversify';
import SERVICES from '../services/services';

// Interfaces
import Auth from '../interfaces/Auth';
import CustomerApiProviderInterface from '../interfaces/CustomerApiProviderInterface';
import Format from '../interfaces/Format';


// Providers
import AuthProvider from '../providers/auth-provider/AuthProvider';
import CustomerApiProvider from '../providers/customer-api-provider/CustomerApiProvider';
import FormatProvider from '../providers/format-provider/FormatProvider';

let IocContainer = new Container();

// Bind Providers to Interfaces
IocContainer.bind<Auth>(SERVICES.AUTH).to(AuthProvider);
IocContainer.bind<CustomerApiProviderInterface>(SERVICES.CUSTOMER).to(CustomerApiProvider);
IocContainer.bind<Format>(SERVICES.FORMAT).to(FormatProvider);

export default IocContainer;
