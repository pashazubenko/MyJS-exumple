import { expect } from 'chai';

import Container from './IocContainer';
import SERVICES from '../services/services';

// Interfaces
import Cookie from '../interfaces/Cookie';
import Auth from '../interfaces/Auth';
import CustomerApiProviderInterface from '../interfaces/CustomerApiProviderInterface';

// Providers
import CookiesProvider from '../providers/cookies-provider/CookiesProvider';
import AuthProvider from '../providers/auth-provider/AuthProvider';
import CustomerApiProvider from '../providers/customer-api-provider/CustomerApiProvider';

describe('IocContainer tests', () => {

    it('Cookie', async () => {
        expect(Container.get<Cookie>(SERVICES.COOKIE)).to.be.an.instanceof(CookiesProvider);
    });

    it('Auth', async () => {
      expect(Container.get<Auth>(SERVICES.AUTH)).to.be.an.instanceof(AuthProvider);
    });

    it('CustomerApiProviderInterface', async () => {
      expect(Container.get<CustomerApiProviderInterface>(SERVICES.CUSTOMER)).to.be.an.instanceof(CustomerApiProvider);
    });
});
