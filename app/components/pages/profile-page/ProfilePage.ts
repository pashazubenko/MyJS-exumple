import Vue from 'vue';
import Component from 'vue-class-component';

import { AppHeader }       from '../../layouts/app-header';
import { AppFooter }       from '../../layouts/app-footer';
import { AppSidebar }      from '../../layouts/app-sidebar';
import { AppBreadcrumbs }  from '../../layouts/app-breadcrumbs';
import { UserProfileForm } from '../../forms/user-profile-form';
import { ApiStatusAlerts } from '../../blocks/api-status-alerts';

import './profile-page.scss';

@Component({
    template: require('./profile-page.html'),
    props: {
        auth: Boolean,
    },
    components: {
      'app-header': AppHeader,
      'app-footer': AppFooter,
      'app-sidebar': AppSidebar,
      'app-breadcrumbs': AppBreadcrumbs,
      'user-profile-form': UserProfileForm,
      'api-status-alerts': ApiStatusAlerts,
    }
})
export class ProfilePage extends Vue {
  get user() {
    return this.$store.getters.user;
  }
  get breadcrumbs() {
    return { name: this.$i18n.t('edit_profile', this.$store.getters.locale)};
  }
}
