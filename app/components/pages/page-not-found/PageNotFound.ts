import Vue from 'vue';
import Component from 'vue-class-component';

import './page-not-found.scss';

@Component({
    template: require('./page-not-found.html')
})
export class PageNotFound extends Vue {

}
