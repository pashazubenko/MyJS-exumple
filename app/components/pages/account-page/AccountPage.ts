import Vue from 'vue';
import Component from 'vue-class-component';

import { AppHeader }       from '../../layouts/app-header';
import { AppFooter }       from '../../layouts/app-footer';
import { AppBreadcrumbs }  from '../../layouts/app-breadcrumbs';

Vue.use(Navbar);

import './account-page.scss';

@Component({
    template: require('./account-page.html'),
    props: {
        auth: Boolean,
    },
    components: {
      'app-header': AppHeader,
      'app-footer': AppFooter,
      'app-breadcrumbs': AppBreadcrumbs,
    }
})
export class AccountPage extends Vue {

}
