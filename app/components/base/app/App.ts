import Vue from 'vue';
import Component from 'vue-class-component';
import { Watch } from 'vue-property-decorator';

import Container from '../../../container/IocContainer';
import SERVICES  from '../../../services/services';

import Cookie                       from '../../../interfaces/Cookie';
import TranslationProviderInterface from '../../../interfaces/TranslationProviderInterface';

@Component({
  template: require('./app.html'),
})
class App extends Vue {

  cookie       = Container.get<Cookie>(SERVICES.COOKIE);
  translations = Container.get<TranslationProviderInterface>(SERVICES.TRANSLATION);

  // Lifecycle hook
  mounted () {
    // Check cookie for language
    if (this.cookie.get('_icl_current_language')) {
      this.$i18n.locale = this.cookie.get('_icl_current_language');
    } else {
      this.$i18n.locale = process.env.DEFAULT_LOCALE;
    }
    this.$store.dispatch('setLocale', this.$i18n.locale);

    this.$store.dispatch('setAuth');
    if (this.auth) {
      this.$store.dispatch('setMenus', this.locale);
      this.$store.dispatch('setCountries');
      this.$store.dispatch('setUser');
    }
  }

  // Users Bookings Watch
  @Watch('user')
  watchUserFunction(new_val, old_val) {
    if (new_val && old_val && this.auth) {
      this.$store.dispatch('setUserSettings');
      this.$store.dispatch('setBookings');
      this.$store.dispatch('setReviews');
    }
  }

  // When locale update
  @Watch('locale')
  watchLocaleFunction(new_val, old_val) {
    if (new_val && old_val && this.auth) {
      this.$store.dispatch('setMenus', new_val);
      this.$store.dispatch('setCountries');
      this.$store.dispatch('setBookings');
      this.$store.dispatch('setReviews');
    }

    if (new_val !== process.env.DEFAULT_LOCALE) {
      this.setTranslations(new_val);
    }

    this.$i18n.locale = new_val; // Should be last for hide load prosess
  }

  get auth() {
    return this.$store.getters.auth;
  }

  get t9n() {
    return this.$store.getters.t9n;
  }

  get user() {
    return this.$store.getters.user;
  }

  get locale() {
    return this.$store.getters.locale;
  }

  setTranslations(locale) {
    this.translations.getTranslation(locale).then(
      (response) => {
        this.$i18n.setLocaleMessage(locale, response.data);
      },
      (error) => {
        console.error(error);
      }
    );
  }
}

export default App;
