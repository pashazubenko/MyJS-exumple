import Vue from 'vue';
import Component from 'vue-class-component';

import './app-breadcrumbs.scss';

@Component({
    template: require('./app-breadcrumbs.html'),
    props: {
        current: Object,
    },
})
export class AppBreadcrumbs extends Vue {
    current;

    get currentName() {
        return this.current.name;
    }
}
