import Vue from 'vue';
import Component from 'vue-class-component';

import Container from '../../../container/IocContainer';
import SERVICES  from '../../../services/services';

import Auth                         from '../../../interfaces/Auth';
import TranslationProviderInterface from '../../../interfaces/TranslationProviderInterface';
import Cookie                       from '../../../interfaces/Cookie';

import './app-header.scss';

@Component({
    template: require('./app-header.html'),
    props: {
        auth: Boolean,
    },
})
export class AppHeader extends Vue {
    authProvider = Container.get<Auth>(SERVICES.AUTH);
    cookie  = Container.get<Cookie>(SERVICES.COOKIE);
    locales = {
        en: 'English',
        nl: 'Nederlands',
        de: 'Deutsch',
        es: 'Español',
    };
    displayLeftSidebar = false;
    displayRightSidebar = false;

    get primary_menu() {
        return this.$store.getters.menus.primary_menu;
    }

    get localeCode() {
        return this.$store.getters.locale;
    }

    get locale() {
        return '<a class="flag" href="#"><img height="14" src="' + process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/' + this.$store.getters.locale + '-lang-icon.png" alt=""><span>' + this.locales[this.$store.getters.locale] + '</span> </a>';
    }

    get localeMobMenu() {
        return '<img height="20" src="' + process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/' + this.$store.getters.locale + '-lang-icon.png" alt=""><span> Change language </span><i class="fa fa-angle-right" aria-hidden="true"></i>';
    }

    get localeMob() {
        return '<i class="fa fa-align-justify"></i><div class="current-flag" style="background: url(' + process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/' + this.$store.getters.locale + '-lang-icon.png) no-repeat center / cover;"></div>';
    }

    get aboutUsIcon() {
        return process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/about-us-icon.png';
    }

    get contactIcon() {
        return process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/contact-icon-top.png';
    }

    get nlLangIcon() {
        return process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/nl-lang-icon.png';
    }

    get deLangIcon() {
        return process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/de-lang-icon.png';
    }

    get enLangIcon() {
        return process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/en-lang-icon.png';
    }

    get esLangIcon() {
        return process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/es-lang-icon.png';
    }

    get infoIcon() {
        return process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/info-icon.png';
    }

    get supportIcon() {
        return process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/support-icon.png';
    }

    get logoIcon() {
        return process.env.PATH_HOME + process.env.PATH_BASE + 'img/logo-min.png';
    }

    get whatsappIcon() {
        return process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/whatsapp-icon.png';
    }

    leftSidebarMouseleave () {
        this.displayLeftSidebar = false;
    }

    rightSidebarMouseleave () {
        this.displayRightSidebar = false;
    }

    set_locale(locale) {
      this.cookie.set('_icl_current_language', locale);
      this.$store.dispatch('setLocale', locale);
    }

    login() {
        this.authProvider.login();
    }

    logout() {
        this.authProvider.logout();
    }

    hover_on(item, v) {
       this.$refs.primaryMenuDropdown[v].show();
    }
    hover_over(item, v) {
       this.$refs.primaryMenuDropdown[v].hide();
    }
}
