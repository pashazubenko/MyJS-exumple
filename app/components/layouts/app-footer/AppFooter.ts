import Vue from 'vue';
import Component from 'vue-class-component';

import './app-footer.scss';

@Component({
    template: require('./app-footer.html'),
})
export class AppFooter extends Vue {

    get footer_menu() {
        return this.$store.getters.menus.footer_menu;
    }

    calcWidgetClass(key) {
        if (key < 4) {
            return 'd-none d-sm-block';
        } else {
            return '';
        }
    }
}
