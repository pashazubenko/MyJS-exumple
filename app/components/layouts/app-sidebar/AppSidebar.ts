import Vue from 'vue';
import Component from 'vue-class-component';

import './app-sidebar.scss';

@Component({
    template: require('./app-sidebar.html')
})
export class AppSidebar extends Vue {

}
