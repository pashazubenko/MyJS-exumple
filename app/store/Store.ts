import Vue from 'vue';
import Vuex from 'vuex';

import Container from '../container/IocContainer';
import SERVICES  from '../services/services';

import Auth                         from '../interfaces/Auth';
import CustomerApiProviderInterface from '../interfaces/CustomerApiProviderInterface';
import MenuProviderInterface        from '../interfaces/MenuProviderInterface';
import TranslationProviderInterface from '../interfaces/TranslationProviderInterface';

Vue.use(Vuex);

const store = new Vuex.Store({

    // Inner Data Structure
    state: {
      auth: false,
      locale: process.env.DEFAULT_LOCALE,
      bookings: {
        data: [],
        meta: {},
      },
      api: {
        customers: {
          status: null,
          message: '',
        },
        translations: {
          status: null,
          message: '',
        },
        bookings: {
          status: null,
          message: '',
        },
        countries: {
          status: null,
          message: '',
        },
        reviews: {
          status: null,
          message: '',
        }
      },
      reviews: {},
      settings: {},
      menus: {},
      countries: [],
      user: {},
    },

    // Outside communication with data.
    actions: {
      setAuth({commit}, status) {
        if (status) {
          commit('SET_AUTH', status);
        }
        else {
          let auth_provider = Container.get<Auth>(SERVICES.AUTH);
          if (auth_provider.isAuthenticated()) {
            commit('SET_AUTH', true);
          }
          else {
            commit('SET_AUTH', false);
          }
        }
      },

      setMenus({commit}, locale) {
        let menu_provider = Container.get<MenuProviderInterface>(SERVICES.MENUS);
        menu_provider.getMenus(locale).then(
          (response) => {
            commit('SET_MENUS', response.data);
            commit('SET_API_TRANSLATIONS', { status: true, message: 'Online' });
          },
          (error) => {
            commit('SET_API_TRANSLATIONS', { status: false, message: error.message });
          }
        );
      },

      setUser({commit}, user) {
        if (user) {
          commit('SET_USER', user);
        }
        else {
          let customer_provider = Container.get<CustomerApiProviderInterface>(SERVICES.CUSTOMER);
          customer_provider.getUserInfo().then(
            (response) => {
              commit('SET_USER', response.data.data);
              commit('SET_API_CUSTOMERS', { status: true, message: 'Online' });
            },
            (error) => {
              commit('SET_API_CUSTOMERS', { status: false, message: error.message });
            }
          );
        }
      },

      setUserSettings({commit}) {
        let customer_provider = Container.get<CustomerApiProviderInterface>(SERVICES.CUSTOMER);
        customer_provider.getUserSettings().then(
          (response) => {
            commit('SET_USER_SETTINGS', response.data.data.settings);
            commit('SET_API_CUSTOMERS', { status: true, message: 'Online' });
          },
          (error) => {
            commit('SET_API_CUSTOMERS', { status: false, message: error.message });
          }
        );
      },

      setReviews({commit}) {
        let customer_provider = Container.get<CustomerApiProviderInterface>(SERVICES.CUSTOMER);
        customer_provider.getUserReviews().then(
          (response) => {
            commit('SET_REVIEWS', response.data.data);
            commit('SET_API_REVIEWS', { status: true, message: 'Online' });
          },
          (error) => {
            commit('SET_API_REVIEWS', { status: false, message: error.message });
          }
        );
      },

      // List of avalible countrys
      setCountries({commit}) {
        let customer_provider = Container.get<CustomerApiProviderInterface>(SERVICES.CUSTOMER);
        customer_provider.getCountries().then(
          (response) => {
            commit('SET_COUNTRIES', response.data.data);
            commit('SET_API_COUNTRIES', { status: true, message: 'Online' });
          },
          (error) => {
            commit('SET_API_COUNTRIES', { status: false, message: error.message });
          }
        );
      },

      // Set first bookings
      setBookings({commit}) {
        let customer_provider = Container.get<CustomerApiProviderInterface>(SERVICES.CUSTOMER);
        customer_provider.getBookingsHistory().then(
          (response) => {
            commit('SET_BOOKINGS', response.data);
            commit('SET_API_BOOKINGS', { status: true, message: 'Online' });
          },
          (error) => {
            commit('SET_API_BOOKINGS', { status: false, message: error.message });
          }
        );
      },

      // "Load more" method
      loadBookings({commit}, data) {
        let customer_provider = Container.get<CustomerApiProviderInterface>(SERVICES.CUSTOMER);
        customer_provider.getBookingsHistoryPage(data.next).then(
          (response) => {
            commit('LOAD_BOOKINGS', response.data);
            commit('SET_API_BOOKINGS', { status: true, message: 'Online' });
          },
          (error) => {
            commit('SET_API_BOOKINGS', { status: false, message: error.message });
          }
        );
      },

      setLocale({commit}, locale) {
        commit('SET_LOCALE', locale);
      }
    },

    // Logic that change data
    mutations: {
      SET_API_CUSTOMERS(state, { status, message }) {
        state.api.customers.status = status;
        state.api.customers.message = message;
      },

      SET_API_TRANSLATIONS(state, { status, message }) {
        state.api.translations.status = status;
        state.api.translations.message = message;
      },

      SET_API_BOOKINGS(state, { status, message }) {
        state.api.bookings.status = status;
        state.api.bookings.message = message;
      },

      SET_API_COUNTRIES(state, { status, message }) {
        state.api.countries.status = status;
        state.api.countries.message = message;
      },

      SET_API_REVIEWS(state, { status, message }) {
        state.api.reviews.status = status;
        state.api.reviews.message = message;
      },

      SET_AUTH(state, status) {
        state.auth = status;
      },

      SET_MENUS(state, menus) {
        state.menus = menus;
      },

      SET_USER(state, user) {
        state.user = user;
      },

      SET_USER_SETTINGS(state, settings) {
        state.settings = settings;
      },

      SET_REVIEWS(state, reviews) {
        state.reviews = reviews;
      },

      SET_COUNTRIES(state, countries) {
        state.countries = countries;
      },

      SET_BOOKINGS(state, bookings) {
        state.bookings = bookings;
      },

      LOAD_BOOKINGS(state, bookings) {
        state.bookings.data = state.bookings.data.concat(bookings.data);
        state.bookings.meta = bookings.meta;
      },

      SET_LOCALE(state, locale) {
        state.locale = locale;
      }
    },

    // Get Data
    getters: {
      api(state) {
        return state.api;
      },
      auth(state) {
        return state.auth;
      },
      locale(state) {
        return state.locale;
      },
      user(state) {
        return state.user;
      },
      bookings(state) {
        return state.bookings;
      },
      countries(state) {
        return state.countries;
      },
      settings(state) {
        return state.settings;
      },
      reviews(state) {
        return state.reviews;
      },
      menus(state) {
        return state.menus;
      },
    },
});

export default store;
